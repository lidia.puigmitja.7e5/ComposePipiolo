package com.example.composepipiolo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composepipiolo.ui.theme.ComposePipioloTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposePipioloTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MyApp()
                }
            }
        }
    }
}

@Composable
fun MyApp() {
    Scaffold(
        content = {
            Greeting(name = "Liante")
        }
    )
}


@Composable
fun Greeting(name: String) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(all = 16.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {


        Image(
            painter = painterResource(id = R.drawable.kh),
            contentDescription = "Photo of Kingdom Hearts",
            modifier = Modifier
                .padding(start = 16.dp, top = 16.dp, end = 16.dp)
                .size(250.dp)
                .clip(CircleShape)
                .border(2.dp, Color.Gray, CircleShape)
                .background(Color.White)

            //modifier = Modifier.padding(start = 120.dp, top = 200.dp, end = 100.dp)
        )

        Text(
            text = "KINGDOM HEARTS",
            fontSize = 40.sp,
            color = Color(red = 0.5f, green = 0.5f, blue = 1f, alpha = 0.9f),
            fontWeight = FontWeight.Bold,
            fontFamily = FontFamily.Monospace,
            modifier = Modifier.padding(top = 24.dp)
        )
        Text(
            text = "Kingdom Hearts es una historia en la que la luz y la amistad derrotan a la oscuridad.\nProcedimos a mostrar dos botones que no hacen nada, diviertete clickandolos!",
            fontSize = 20.sp,
            fontFamily = FontFamily.Monospace,
            color = Color(red = 0.7f, green = 0.4f, blue = 1f, alpha = 0.9f),
            fontWeight = FontWeight.Medium, textAlign = TextAlign.Justify,
            modifier = Modifier.padding(top = 16.dp)
            // modifier = Modifier.padding(start = 150.dp, top = 150.dp, end = 150.dp)
        )

        //Spacer(modifier = Modifier.size(size = 60.dp))

        Row(
            modifier = Modifier
                .fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.End
        ) {
            Button(
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(
                        red = 0.4f,
                        green = 0.6f,
                        blue = 1f,
                        alpha = 0.9f
                    )
                ),
                border = BorderStroke(
                    2.dp, Color(
                        red = 0.9f,
                        green = 0.6f,
                        blue = 1f,
                        alpha = 0.9f
                    )
                ),
                onClick = {
                    //your onclick code here
                }) {
                Text(
                    text = "Sora",
                    fontSize = 16.sp,
                    fontFamily = FontFamily.Monospace,
                    fontWeight = FontWeight.Bold
                )
            }

            Spacer(modifier = Modifier.size(size = 8.dp))

            Button(colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(
                    red = 0.9f,
                    green = 0.6f,
                    blue = 1f,
                    alpha = 0.9f
                )
            ),
                border = BorderStroke(
                    2.dp, Color(
                        red = 0.3f,
                        green = 0.6f,
                        blue = 1f,
                        alpha = 0.8f
                    )
                ),
                onClick = {
                    //your onclick code here
                }) {
                Text(
                    text = "Roxas",
                    fontSize = 16.sp,
                    fontFamily = FontFamily.Monospace,
                    fontWeight = FontWeight.Bold
                )
            }
        }


    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposePipioloTheme {
        Greeting("Liante")
    }
}